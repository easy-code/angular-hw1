import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AboutPersonComponent} from "./components/aboutpersoncomponent/aboutpersoncomponent.component";
import {SocialIconComponent} from "./components/socialiconcomponent/socialiconcomponent.component";

@NgModule({
  declarations: [
    AppComponent,
    AboutPersonComponent,
    SocialIconComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
