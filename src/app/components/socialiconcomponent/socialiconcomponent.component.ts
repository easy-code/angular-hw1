import {Component} from "@angular/core";

// Interface
import {Social} from "../models/Social";

@Component({
  selector: 'app-socialicon',
  templateUrl: './socialiconcomponent.component.html',
  styleUrls: ['./socialiconcomponent.component.css']
})

export class SocialIconComponent {
  socials: Social[] = [
    {
      nameOfClass: 'facebook',
      link: 'https://www.facebook.com/anovichenkoX1'
    },
    {
      nameOfClass: 'twitter',
      link: 'https://twitter.com/anovichenko9'
    },
    {
      nameOfClass: 'instagram',
      link: 'http://instagram.com/_u/anovichenko/'
    },
    {
      nameOfClass: 'skype',
      link: 'skype:ds_dark_l0rd?chat'
    }
  ];
}
