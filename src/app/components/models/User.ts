export interface User {
  name: string,
  age?: number,
  nationality: string,
  phone: string,
  email: string
}
