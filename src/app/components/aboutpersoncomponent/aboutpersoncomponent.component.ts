import {Component} from "@angular/core";

// Interface
import { User } from "../models/User";

@Component({
  selector: 'app-aboutperson',
  templateUrl: './aboutpersoncomponent.component.html',
  styleUrls: ['./aboutpersoncomponent.component.css']
})

export class AboutPersonComponent {
  avatarUrl = 'assets/avatar.jpg';

  show_info: boolean = true;

  name: string = 'Alexander';

  user: User = {
    name: 'Alexander Novichenko',
    age: 30,
    nationality: 'Ukrainian',
    phone: '+38 093 66 06 027',
    email: 'anovichenko.dev@gmail.com'
  };
}
